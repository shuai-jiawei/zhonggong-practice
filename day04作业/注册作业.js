// 1、已知persons.json用户信息：

// 2、现有 let user = { username: "admin1", password: "12345" }


// 注册原理：创建 register.js文件：
// 如果persons中没有对应的用户，则把user写入到persons.json中，提示注册成功
// 如果有对应的用户名，则提示用户名已经被占用

// 登录原理：创建login.js文件
// 如果persons.json中含有对应的用户名和密码则提示登录成功，否则提示用户名和密码错误

const fs = require('fs') //引入fs功能
let value = fs.readFileSync('persons.json').toString()
value = JSON.parse(value)


console.log(value);

let user = { username: "admin1", password: "12345" }
// let user = {"username":"xm3","password":"333"}

// 注册
let tag = value.find(item => item.username === user.username)
if (tag) {
    console.log('该用户已注册');

    //  document.write('该用户已注册')
} else {
    console.log('注册成功');
    //  document.write('注册成功')
    value.push(user)
    fs.writeFileSync('persons.json',JSON.stringify(value),{ flag:'w' })
    // console.log(value);

}