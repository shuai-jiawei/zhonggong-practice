// 1、已知persons.json用户信息：

// 2、现有 let user = { username: "admin1", password: "12345" }


// 注册原理：创建 register.js文件：
// 如果persons中没有对应的用户，则把user写入到persons.json中，提示注册成功
// 如果有对应的用户名，则提示用户名已经被占用

// 登录原理：创建login.js文件
// 如果persons.json中含有对应的用户名和密码则提示登录成功，否则提示用户名和密码错误

const fs = require('fs') //引入fs功能
let value = fs.readFileSync('persons.json').toString()
value = JSON.parse(value)
let user = { username: "admin1", password: "12345" }
// let user = {"username":"xm3","password":"333"}
// 登录
let admin = value.find(item => user.username === item.username && user.password === item.password)
if (admin) {
    console.log('登录成功');
    //  document.write('登录成功')
} else {
    console.log('用户名或密码错误');
    //  document.write('用户名或密码错误')   
}
