
module.exports = str => str.replace(/^\s+|\s+$/g, '');

module.exports.left = str => str.replace(/^\s+/g, '');

module.exports.right = str => str.replace(/\s+$/g, '');

