const express = require('express')
const path = require('path')
const DetectUser = require('../js/DetectUser')
const router = express.Router()
router.get('/', (req, res) => {
    let filepath = path.join(__dirname, '../index.html')
    res.sendFile(filepath)
})
router.get('/views/login', (req, res) => {
    // console.log(req.params);
    let filepath = path.join(__dirname, '../views/login.html')
    res.sendFile(filepath)
})
router.get('/views/register', (req, res) => {
    let filepath = path.join(__dirname, '../views/register.html')
    res.sendFile(filepath)
})
router.get('/views/loginp', (req, res) => {
    console.log(req.query);
    res.send(DetectUser.login(req.query))
    // let filepath= path.join(__dirname,'../views/login.html')
    // res.sendFile(filepath)
})
router.get('/views/registerp', (req, res) => {
    // let filepath= path.join(__dirname,'../views/register.html')
    // res.sendFile(filepath)
    res.send(DetectUser.register(req.query))
})

module.exports = router