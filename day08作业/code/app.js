const express = require('express') //引入框架
const serveFavicon = require('serve-favicon') //引入小图标的第三方包
const router = require('./router/router')
const postRouter = require('./router/postRouter')
const path = require('path')
const fs = require('fs')

const app = express()


app.listen(3000,function(){
console.log('3000端口开启了，可以访问: http://localhost:3000')
})
// 用模块方法显示小图标
app.use(serveFavicon(path.join(__dirname,'favicon.ico')))
// 开启post 
app.use(express.urlencoded( {extended:false} ) )
//开放静态资源
app.use('/static',express.static('static') )
app.use('/views', router)
app.use('/post', postRouter)
app.get('/',(req,res)=>{
    let filepath= path.join(__dirname,'/views/login.html')
    
     res.sendFile(filepath)
 })

