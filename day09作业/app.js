//使用nodemon app.js 我们项目就运行起来了。

const express = require('express')
const app = express()
app.listen(3000, function () {
    console.log('3000端口开启了，可以访问: http://localhost:3000')
})
const formidable = require('formidable');

const path = require('path') //join
const fs = require('fs')
//引入小图标
const favicon = require('serve-favicon')

app.set('view engine', 'ejs')
//开放静态资源
app.use('/static', express.static('static'))

//设置小图标  如果未出来 强制刷新一下 CTRL+F5
app.use(favicon('favicon.ico'))
//接收post参数的中间件
app.use(express.urlencoded({ extended: false }))

//moment或time-stamp
//引入模块
const timeStamp = require('time-stamp')
// const moment = require('moment');


app.get('/', (req, res) => {
    res.send('我是网站的首页')
})


// get :  login  ->   渲染登录表单页面
app.get('/login', (req, res) => {
    // let filepath = path.join(__dirname, 'views/login.html')
    res.render('login')
})

// get ： register -> 渲染注册表单页面
app.get('/register', (req, res) => {
    // let filepath = path.join(__dirname, 'views/register.html')
    // res.sendFile(filepath)
    res.render('register')
})

// get ： register -> 渲染注册表单页面
app.get('/users', (req, res) => {
    let filepath = path.join(__dirname, 'views/users.html')
    res.render('users')
})
// http://localhost:3000/uploads/20211019202624807530.jpg
app.use('/uploads', (req, res) => {
   
  let filepath=  path.join(__dirname,'/uploads'+req.url)
    console.log(filepath);
    res.sendFile(filepath)
})
//真正的实现登录功能
app.post('/login', (req, res) => {

    let user = req.body //接收用户传递过来的参数
    //res.send('我要实现登录' )
    let persons = fs.readFileSync('./persons.json', { encoding: 'utf-8' });
    persons = JSON.parse(persons);

    //2. 查找有无匹配的条件的数据  find/findIndex

    let userIndex = persons.findIndex(item => item.username === user.tel && item.password === user.pass)

    if (userIndex == -1) {
        res.send('用户名或密码错误')
    } else {
        console.log(typeof persons ,persons);
        res.render('users' ,{persons})
    }
    // res.send( req.query )
})

//实现注册
app.post('/register', (req, res) => {

    const form = formidable({
        uploadDir: 'uploads'
    })

    form.parse(req, (err, value, filesval) => {
        //1. 接收参数
        let { username = '', password = '', repwd = '' } = value // 对象的解构赋值
        //去除左右空格
        let reg = /^\s+|\s+$/g
        username = username.replace(reg, '')

        //2. 必须做一些数据验证
        //2.1 是否为空
        if (username == '' || !password || !repwd) {
            res.send('请传递必要参数')
            return //都已经send响应结果了，没必要再往下执行
        }
        //2. 格式判断： 长度
        //用户名不到小于5位 不能大于 15位
        if (username.length < 5 || username.length > 15) {
            res.send('用户名不到小于5位 不能大于 15位')
            return
        }
        //两次密码不一致
        if (password != repwd) {
            res.send('两次密码不一致')
            return
        }

        //3. 注册的逻辑判断
        let persons = fs.readFileSync('persons.json', 'utf8')
        // console.log( persons,'这是string' )
        persons = JSON.parse(persons) //把jsnon字符串转换成对象

        //user 用户访问某个表单，传递过来的
        // let user = { username: "zs123", password: "222" }

        let index = persons.findIndex(item => { //如果是想遍历数组中的所有数据，只能用forEach
            // console.log( 123 )
            return item.username == username  //当找到了，则不会向下遍历 了
        })

        // console.log( index,'是否找到' )


        if (index != -1) {
            res.send('用户名太香了~')
            return
        } else {
            let name = filesval.f1.name
            let suff = path.extname(name)
            let arrsuff = ['.jpg', '.png', '.jpeg', '.svg', '.webp', '.gif']
            let tag = arrsuff.find((item) => item === suff)
            if (tag === undefined) {
                res.send('文件不符合要求')
                return
            }
            let time = timeStamp('YYYYMMDDHHmmss')
            let oldpath = filesval.f1.path
            let num = (Math.random() + '').slice(2, 8)
            let newpath = 'uploads/' + time + num + suff
            fs.renameSync(oldpath, newpath)
            persons.push({ username, password, time, avatar: newpath })
            //写入到persons.json文件中
            fs.writeFileSync('persons.json', JSON.stringify(persons))//再转换成json字符串

            res.render('users', {
                persons
                //  newpath, 
                //  username, 
                //  password, 
                //  year:time.slice(0,4), 
                //  month:time.slice(4,6), 
                //  day:time.slice(6,8), 
                //  hour:time.slice(8,10), 
                //  min:time.slice(10,12), 
                //  s:time.slice(12), 
                })
            return
        }


    })




    // res.send('实现注册的')
})