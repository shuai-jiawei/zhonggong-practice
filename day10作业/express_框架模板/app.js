//不建议使用，看起来不友好 。
// var createError = require('http-errors');


//express
var express = require('express');
//path
var path = require('path');

//cookie
var cookieParser = require('cookie-parser');

var logger = require('morgan');

//路由
var indexRouter = require('./routes/index');

//实例化app
var app = express();

// view engine setup 设置ejs
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


// app.use(logger('dev')); //每次请求会在控制输出一个日志

//开启post中间件
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//开启cookie
app.use(cookieParser());

//开启静态
app.use('/public',express.static(path.join(__dirname, 'public')));

//使用路由
app.use('/', indexRouter);





// catch 404 and forward to error handler
app.use(function(req, res, next) {
  //next(createError(404));
  res.status(404).send('my is 404 page')
});

// error handler 统一数据处理
app.use(function(err, req, res, next) {

  res.send( '这是统一数据处理' )

  // set locals, only providing error in development
  // res.locals.message = err.message;
  // res.locals.error = req.app.get('env') === 'development' ? err : {};

  // // render the error page
  // res.status(err.status || 500);
  // res.render('error');
});

module.exports = app;
