//引入mysql
const mysql = require('mysql')

let connObj = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    port: '3306',
    database: 'stu'
})

function mydbsql (sql){
  return new Promise( (reslvle)=>{
        connObj.query(sql, (err, results) => {
            reslvle({err,results})
        })
    })
    
}
module.exports = mydbsql
