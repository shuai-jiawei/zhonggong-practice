/* 
    首页
    顶部banner    
*/
window.onload = function () {
    var ban = document.getElementsByClassName('banner')[0];
    var banul = ban.getElementsByClassName('list')[0];
    var banimg = ban.getElementsByClassName('img')[0];
   
    var banlis = banul.getElementsByTagName('li');
    var imglis = banimg.getElementsByTagName('li');
    console.log(banlis);
    banlis[0].className = 'high';
    imglis[0].className = 'active';

    // 轮播图
    var n = 0;
    ban.timer = setInterval(auto, 2000);

    function auto() {
        n++;
        if (n == banlis.length) n = 0;
        for (var i = 0; i < banlis.length; i++) {
            banlis[i].className = '';
            // imglis[i].className = '';
            buffMove(imglis[i], {
                opacity: 0
            });
        }
        banlis[n].className = 'high';
        // imglis[n].className = 'active';
        buffMove(imglis[n], {
            opacity: 1
        });

    }

    // 划上停止
    banul.onmouseenter = function () {
        clearInterval(ban.timer);
    }

    // 划下开始
    banul.onmouseleave = function () {
        ban.timer = setInterval(auto, 2000);
    }

    // 划上每一个切换
    for (var j = 0; j < banlis.length; j++) {
        banlis[j].index = j;
        banlis[j].onmouseenter = function () {
            n = this.index - 1;
            auto();
        }
    }
    
    var fix = document.getElementsByClassName('fix')[0];
    console.log(ban.offsetLeft);
    var cw = document.documentElement.clientWidth;
    if (cw > 1270) {
        fix.style.left = ban.offsetLeft + 1200 + 'px';
    } else {
        fix.style.left = auto;
        fix.style.right = 0;
    }
    // 返回顶部
    var top1 = fix.querySelector('.top');
    console.log(top1);
    top1.onclick = function () {
        document.body.timer = setInterval(function () {
            document.documentElement.scrollTop -= 50;
            if (document.documentElement.scrollTop <= 0) {
                clearInterval(document.body.timer);
            }
        }, 1);
    }
   
    
}