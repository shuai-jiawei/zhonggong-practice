import Vue from 'vue'
import VueRouter from 'vue-router'
import contact from '../pages/contact.vue';

Vue.use(VueRouter)

const routes = [
  {path:"/contact",component:contact},
  {path:"*",redirect:"/contact"}
  
]

const router = new VueRouter({
 
  routes
})

export default router
